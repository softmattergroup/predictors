CXX=g++
CXXFLAGS=-O3 -std=c++17 -mtune=native -march=native -Wall

SRC=src

Predictors3D : src/Main.cpp src/CoarseGrainedPDB.h
	$(CXX) -o Predictors3D src/Main.cpp $(CXXFLAGS)

#all : volume surfacearea inertia dipole mapper curvature charge

#volume : $(SRC)/volume.cc $(SRC)/cg_pdb.h
#	$(CXX) -o volume $(SRC)/volume.cc $(CXXFLAGS)

#surfacearea : $(SRC)/surfacearea.cc $(SRC)/cg_pdb.h
#	$(CXX) -o surfacearea $(SRC)/surfacearea.cc $(CXXFLAGS)

#inertia : $(SRC)/inertia.cc $(SRC)/cg_pdb.h
#	$(CXX) -o inertia $(SRC)/inertia.cc $(CXXFLAGS)

#dipole : $(SRC)/dipole.cc $(SRC)/cg_pdb.h
#	$(CXX) -o dipole $(SRC)/dipole.cc $(CXXFLAGS)

#mapper : $(SRC)/mapper.cc $(SRC)/cg_pdb.h
#	$(CXX) -o mapper $(SRC)/mapper.cc $(CXXFLAGS)

#curvature : $(SRC)/curvature.cc $(SRC)/cg_pdb.h
#	$(CXX) -o curvature $(SRC)/curvature.cc $(CXXFLAGS)

#charge : $(SRC)/charge.cc $(SRC)/cg_pdb.h
#	$(CXX) -o charge $(SRC)/charge.cc $(CXXFLAGS)

clean:
	rm -f volume surfacearea inertia mapper curvature charge dipole *.o core
