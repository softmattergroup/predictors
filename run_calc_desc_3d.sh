#! /bin/bash

echo "NAME,INERTIA_X,INERTIA_Y,INERTIA_Z,DIPOLE_X,DIPOLE_Y,DIPOLE_Z,SURFACE_AREA,VOLUME,CHARGE_GINI,CURVATURE_GINI,ROUNDNESS"

DIR="/home/ianrouse/DaphniaProteome/rotated_pdbs/InputStructuresr"

for PDBPATH in $(find ${DIR} -name '*.pdb')
do
    COL=$(echo ${PDBPATH} | awk -F/ '{printf NF}')
    ROOT=$(echo ${PDBPATH} | awk -v col=${COL} -F/ '{printf $col}')
    FILENAME=$(echo ${ROOT} | awk -F. '{printf $1}')

    echo "${FILENAME},$(./inertia ${PDBPATH}),$(./dipole ${PDBPATH}),$(./surfacearea ${PDBPATH}),$(./volume ${PDBPATH}),$(./charge ${PDBPATH}),$(./curvature ${PDBPATH})"
done

