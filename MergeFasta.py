'''
Rotate a protein from the initial co-ordinates to principal-axis-aligned form, with the longest axis along Z and the second longest along Y
The final rotation convention is set such that the x-component of the dipole moment is negative
This has the benefit that the UA rotation angles of theta = 90 aligns the longest part of the protein with the surface of the NP
'''



from Bio.PDB.PDBParser import PDBParser
from Bio.PDB.PDBIO import PDBIO
from Bio import SeqIO

from Bio.PDB import MMCIFParser
import numpy.random as npr
import Bio.PDB.Superimposer as Superimposer
import warnings
from Bio import BiopythonWarning


import os
import numpy as np
import numpy.linalg as npla

import argparse
parser = argparse.ArgumentParser(description="Parameters for prediction")

parser.add_argument("-i","--initial", type=int,default=0, help="First sequence for multistepping.")
parser.add_argument("-s","--step", type=int,default=1, help="Stride length for multistepping.")
args = parser.parse_args()


warnings.simplefilter('ignore',BiopythonWarning)

seq0 = args.initial
seqStep = args.step

def SearchDirectory(path, ext=".pdb"):
    files = os.listdir(path)
    pdbs  = []
    for handle in files:
        abspath = os.path.join(path, handle)
        if os.path.isdir(abspath):
            pdbs += SearchDirectory(abspath)
        elif abspath[-(len(ext)):] == ext:
            pdbs.append(abspath)
    return pdbs

def listDiff(mainSet,subSet):
    subSet = set(subSet)
    return [item for item in mainSet if item not in subSet]

def zeroCOM(coordArr):
    coords = np.copy(coordArr)
    coords[:,0] = coords[:,0] - np.mean(coords[:,0])
    coords[:,1] = coords[:,1] - np.mean(coords[:,1])
    coords[:,2] = coords[:,2] - np.mean(coords[:,2])
    return coords

def getTransformMatrix(coordArr):
    ixx = np.sum(coordArr[:,1]**2 + coordArr[:,2]**2)
    ixy = np.sum(- coordArr[:,0]*coordArr[:,1])
    ixz = np.sum(-coordArr[:,0]*coordArr[:,2])
    iyy = np.sum(coordArr[:,0]**2 + coordArr[:,2]**2)
    iyx = ixy
    iyz = np.sum(-coordArr[:,1]*coordArr[:,2])
    izz = np.sum(coordArr[:,0]**2 + coordArr[:,1]**2)
    izx = ixz
    izy = iyz
    inertialArray = np.array([ [ixx, ixy,ixz],[iyx,iyy,iyz],[izx,izy,izz] ])
    eigvals,eigvecs = npla.eig(inertialArray)
    #invarr = npla.inv(eigvecs)
    sortIndex = eigvals.argsort()[::-1]
    invarr = npla.inv(eigvecs[:,sortIndex])
    return invarr


parser = PDBParser(PERMISSIVE=1)
sup = Superimposer()
io = PDBIO()

cifparser = MMCIFParser()
targetFolders = [ "fasta_out"]

#targetFolders = ["pdbs/ExtraStructures"]
allTargets0  = []
numDone  = 0


for targetFolder in targetFolders:
    targetsFound = SearchDirectory(targetFolder,".fasta")
    for targetFound in targetsFound:
        allTargets0.append(targetFound)
allTargets = sorted(  allTargets0   )
print(allTargets)
combinedFasta = open("allfasta.fasta","w")
for target in allTargets:
    fastaIn = open(target,"r")
    for line in fastaIn:
        combinedFasta.write(line)
    fastaIn.close()
combinedFasta.close()        

