'''
Merge together the tables generated from all the individual pepstats tools
'''




import os
import numpy as np
import numpy.linalg as npla

targetSet = open("pdbset.txt","r")



pepstatData = open("DaphniaPepstatsOutput.csv","r")
surfacePepstat = open("proteins3D.csv","r")

spsHeader = surfacePepstat.readline()

structureDataDict = {}
for i in range(5):
    structureOut = open("structure_data_"+str(i)+".csv","r")
    structureFileHeader = structureOut.readline()
    for line in structureOut:
        structureOutLine = line.strip().split(",")
        for i in range(len(structureOutLine)):
            structureOutLine[i] = structureOutLine[i].strip()
        
        structureDataDict[ structureOutLine[0] ] = structureOutLine
        #print(structureOutLine[0]  )


pepstatHeader=pepstatData.readline()
pepstatDataDict = {}

totalHeader = "Name,"+structureFileHeader.strip()+","+spsHeader.strip()+","+pepstatHeader.strip()
for line in pepstatData:
    pepstatDataLine = line.strip().split(",")
    for i in range(len(pepstatDataLine)):
        pepstatDataLine[i] = pepstatDataLine[i].strip()
    pepstatDataDict[pepstatDataLine[0] ] =pepstatDataLine
    #print(pepstatDataLine[0])


print(totalHeader)
for line in targetSet:
    targetSetLineTerms = line.strip().split("/")
    pdbName = targetSetLineTerms[-1].split(".")[0]
    spsLine = surfacePepstat.readline()
    try:
        sequenceData = structureDataDict[ pdbName]
    except:
        print("Sequence data not found: ", pdbName)
        continue
    try:
        pepstatData = pepstatDataDict[ pdbName]
    except:
        print("Pepstat data not found", pdbName)
        continue
    outStr1= ",".join([ str(a) for a in sequenceData])
    outStr2 = ",".join([ str(a) for a in pepstatData])
    print( pdbName.strip()+","+outStr1.strip() +","+spsLine.strip()+","+outStr2.strip())
    #break
structureOut.close()
