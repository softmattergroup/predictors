#! /bin/bash

FASTADIR="../fasta_out"
OUT="pepstats"

for FILENAME in $(find ${FASTADIR} -name "*.fasta")
do
    COL=$(echo ${FILENAME} | awk -F/ '{printf NF}')
    ROOT=$(echo ${FILENAME} | awk -v col=${COL} -F/ '{printf $col}')
    NAME=$(echo ${ROOT} | awk -F. '{printf $1}')
    pepstats -sequence ${FILENAME} -outfile "${OUT}/${NAME}.pepstats"
done

