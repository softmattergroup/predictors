import numpy as np
import pandas as pd
import sklearn.cluster as skcluster
import sklearn.decomposition as skdecomp
from scipy.cluster.vq import vq
import warnings
import os
import shutil

serumTargetMassConc = 1 #g/mol

warnings.simplefilter(action='ignore', category=pd.errors.PerformanceWarning)
proteinSet = pd.read_csv("DaphniaProteinStats.csv",skipinitialspace=True)
print(proteinSet.keys().tolist() )

proteinLogDescriptors =  ["maxAspectRatio","coilFactor","xlength","ylength","zlength","Mass (Da)","Surface Area (A2)","Volume (A3)","Sphericity","First Principle Moment Of Inertia (Da A2)","Second Principle Moment Of Inertia (Da A2)","Third Principle Moment Of Inertia (Da A2)","Surface Amino Acids","Surface Amino Acids %","Surface ALA","Surface ALA %","Surface ALA Dayhoff %","Surface ARG","Surface ARG %","Surface ARG Dayhoff %","Surface ASN","Surface ASN %","Surface ASN Dayhoff %","Surface ASP","Surface ASP %","Surface ASP Dayhoff %","Surface CYS","Surface CYS %","Surface CYS Dayhoff %","Surface GLN","Surface GLN %","Surface GLN Dayhoff %","Surface GLU","Surface GLU %","Surface GLU Dayhoff %","Surface GLY","Surface GLY %","Surface GLY Dayhoff %","Surface HIS","Surface HIS %","Surface HIS Dayhoff %","Surface ILE","Surface ILE %","Surface ILE Dayhoff %","Surface LEU","Surface LEU %","Surface LEU Dayhoff %","Surface LYS","Surface LYS %","Surface LYS Dayhoff %","Surface MET","Surface MET %","Surface MET Dayhoff %","Surface PHE","Surface PHE %","Surface PHE Dayhoff %","Surface PRO","Surface PRO %","Surface PRO Dayhoff %","Surface SER","Surface SER %","Surface SER Dayhoff %","Surface THR","Surface THR %","Surface THR Dayhoff %","Surface TRP","Surface TRP %","Surface TRP Dayhoff %","Surface TYR","Surface TYR %","Surface TYR Dayhoff %","Surface VAL","Surface VAL %","Surface VAL Dayhoff %","Surface Tiny","Surface Tiny %","Surface Small","Surface Small %","Surface Aliphatic","Surface Aliphatic %","Surface Aromatic","Surface Aromatic %","Surface Non-Polar","Surface Non-Polar %","Surface Polar","Surface Polar %","Surface Charged","Surface Charged %","Surface Basic","Surface Basic %","Surface Acidic","Surface Acidic %","MolecularWeight","Residues","AverageResidueWeight","MolarExtinctionCoefficientsReduced","MolarExtinctionCoefficientsCB","ExtinctionCoefficientsReduced","ExtinctionCoefficientsCB","AlaNumber","AlaMole","AlaDayhoffStat","CysNumber","CyseMole","CysteineDayhoffStat","AspNumber","AspMole","AspDayhoffStat","GluNumber","GluMole","GluDayhoffStat","PheNumber","PheMole","PheDayhoffStat","GlyNumber","GlyMole","GlyDayhoffStat","HisNumber","HisMole","HisDayhoffStat","IleNumber","IleMole","IleDayhoffStat","LysNumber","LysMole","LysDayhoffStat","LeuNumber","LeuMole","LeuDayhoffStat","MetNumber","MetMole","MetDayhoffStat","AsnNumber","AsnMole","AsnDayhoffStat","ProNumber","ProMole","ProDayhoffStat","GlnNumber","GlnMole","GlnDayhoffStat","ArgNumber","ArgMole","ArgDayhoffStat","SerNumber","SerMole","SerDayhoffStat","ThrNumber","ThrMole","ThrDayhoffStat","ValNumber","ValMole","ValDayhoffStat","TrpNumber","TrpMole","TrpDayhoffStat","TyrNumber","TyrMole","TyrDayhoffStat","TinyNumber","TinyMole","SmallNumber","SmallMole","AliphaticNumber","AliphaticMole","AromaticNumber","AromaticMole","NonPolarNumber","NonPolarMole","PolarNumber","PolarMole","ChargedNumber","ChargedMole","BasicNumber","BasicMole","AcidicNumber","AcidicMole"
]
pldNames = []

proteinSet["AverageCharge"] = proteinSet["Charge"]/proteinSet["Residues"]
for pld in proteinLogDescriptors:
    proteinSet[pld+"_log"] = proteinSet[pld].apply(np.log1p)
    pldNames.append(pld+"_log")
proteinNumericDescriptors =pldNames + ["First Dipole Moment (Z A)","Second Dipole Moment (Z A)","Third Dipole Moment (Z A)","Charge","AverageCharge","ProbabilityOfExpressionInInclusionBodies","IsoelectricPoint","highestBFactor","lowestBFactor","averageBFactor","disorderFrac"]


standardDescriptors = []
for pnd in proteinNumericDescriptors:
    proteinSet[pnd+"_standard"] = (proteinSet[pnd] - np.mean( proteinSet[pnd] ))/np.std( proteinSet[pnd] )
    standardDescriptors.append( pnd+"_standard" )
print( proteinSet.columns[ proteinSet.isna().any()  ].tolist()  )
numPCAComponents =4
numClusters = 20

proteinPCABuilder = skdecomp.PCA(n_components= "mle")
proteinPCAComponents = proteinPCABuilder.fit_transform(proteinSet[standardDescriptors])

proteinKmeans = skcluster.KMeans(  n_clusters = numClusters, random_state = 0 ).fit(proteinPCAComponents  )
proteinSet["KMeans_mle_cluster"] = proteinKmeans.labels_

proteinSet.to_csv("proteins_clustered.csv")

closest, distances = vq(proteinKmeans.cluster_centers_, proteinPCAComponents  )
proteinSampleSet = proteinSet.iloc[closest].copy()

print(proteinSampleSet)


totalmass = np.sum(proteinSampleSet["Mass (Da)"].to_numpy())

proteinSampleSet.to_csv("protein_exemplers.csv")

proteinNames = proteinSampleSet["Name"].tolist()

#copy out the structures
os.makedirs("selected_proteins",exist_ok=True)
for pn in proteinNames:
    shutil.copyfile("rotated_pdbs/InputStructures/"+pn+".pdb", "selected_proteins/"+pn+".pdb")
    
    
for serumTargetMassConc in [0.1, 1.0, 10.0]:
    serumOut = open("daphnia_serum_mass"+str(serumTargetMassConc)+"_fixedmassfrac.csv","w")
    serumOut.write("#ProteinID,numberconc\n")
    serumOut2 = open("daphnia_serum_mass"+str(serumTargetMassConc)+"_fixednumberfrac.csv","w")
    serumOut2.write("#ProteinID,numberconc\n")
    proteinSampleSet["numberconcequalmass"] = serumTargetMassConc/( numClusters * proteinSampleSet["Mass (Da)"])
    for pn in proteinNames:
        numberConcVal = proteinSampleSet.loc[ proteinSampleSet["Name"] ==pn , "numberconcequalmass"].to_numpy()
        print(pn,  numberConcVal[0])
        serumOut.write(pn+","+str(numberConcVal[0]) + "\n")
        serumOut2.write(pn+","+str(serumTargetMassConc/totalmass) + "\n")
    serumOut.close()
    serumOut2.close()
    
