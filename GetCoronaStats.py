import numpy as np
import pandas as pd
import sklearn.cluster as skcluster
import sklearn.decomposition as skdecomp
from scipy.cluster.vq import vq
import warnings
import os
import shutil


warnings.simplefilter(action='ignore', category=pd.errors.PerformanceWarning)
proteinSet = pd.read_csv("DaphniaProteinStats.csv",skipinitialspace=True)
#print(proteinSet.keys().tolist() )

coronaStatsFile = open("/path/to/UA/results/daphnia-fingerprint-gold/demo.txt","r")

serumProteinConcs = []
coronaSerumProteinFile = "/path/to/UA/CoronaKMCInput/daphnia_serum_mass1.0_fixedmassfrac.csv"

if coronaSerumProteinFile!= "":
    serumFile = open(coronaSerumProteinFile,"r")
    serumFile.readline()
    for line in serumFile:
        if line[0]=="#":
            continue
        lineTerms = line.strip().split(",")
        serumProteinConcs.append(float(lineTerms[1]))
serumProteinConcs = np.array(serumProteinConcs)




headerLine = coronaStatsFile.readline()
headerTerms = headerLine.strip().split(",")
proteinSetTemp = headerTerms[1:-2]
proteinSetNames = []

targetDescriptors = ["Mass (Da)", "Charge", 'AlaNumber',  'CysNumber',  'AspNumber', 'GluNumber',  'PheNumber',  'GlyNumber',  'HisNumber', 'IleNumber', 'LysNumber',  'LeuNumber','MetNumber', 'AsnNumber',  'ProNumber',  'GlnNumber', 'ArgNumber',  'SerNumber', 'ThrNumber',  'ValNumber',  'TrpNumber',  'TyrNumber',  'TinyNumber',  'SmallNumber',  'AliphaticNumber', 'AromaticNumber', 'NonPolarNumber', 'PolarNumber','ChargedNumber',  'BasicNumber','AcidicNumber']



totalSerumMass = 0
massWeightedDescriptors = np.zeros( len(targetDescriptors) )
proteinMassList = []

proteinDescriptorVals = []
for protein in proteinSetTemp:
    proteinTerms = protein.split("-AF-")
    proteinSetNames.append(proteinTerms[0])
    proteinMass = proteinSet.loc[   proteinSet["Name"] == proteinTerms[0], "Mass (Da)"].to_numpy()[0]
    proteinMassList.append(proteinMass)
    proteinDescriptors = proteinSet.loc[   proteinSet["Name"] == proteinTerms[0], targetDescriptors].to_numpy()[0]
    #print(proteinTerms[0], proteinDescriptors)
    proteinDescriptorVals.append(proteinDescriptors)
    massWeightedDescriptors += proteinDescriptors*proteinMass
    totalSerumMass +=  proteinMass
#print(proteinSetNames)
allDescriptorVals = np.stack( proteinDescriptorVals)
#print(allDescriptorVals)
proteinMassArr = np.array(proteinMassList)


massDotConc = np.dot(proteinMassArr, serumProteinConcs)
descriptorsDotConc = np.dot( np.transpose(allDescriptorVals), serumProteinConcs)

unitVector = np.ones_like(serumProteinConcs)
totalConc = np.sum(serumProteinConcs)
massDotUnity = np.dot(proteinMassArr, unitVector)
descriptorsDotUnity = np.dot( np.transpose(allDescriptorVals), unitVector)

print("t," + ",".join([ str(a) for a in targetDescriptors])  +"," +   ",".join([ "enrich_"+str(a) for a in targetDescriptors])     +"," +   ",".join([ "massenrich_"+str(a) for a in targetDescriptors])   )
for line in coronaStatsFile:
    lineTerms = line.strip().split()
    #print(lineTerms[0])
    proteinNums = np.array( [float(a) for a in lineTerms[1:-2]] )
    #print(proteinNums)
    descriptorNums =  np.dot( np.transpose(allDescriptorVals), proteinNums)
    #print( str(lineTerms[0]) +"," + )
    totalAdsorbedMass = np.dot( proteinNums, proteinMassArr)
    totalAdsorbedNumber = np.sum(proteinNums)
    massEnrichmentVals = (descriptorNums * massDotConc)/(0.001+totalAdsorbedMass * descriptorsDotConc)
    enrichmentVals = (descriptorNums * totalConc )/(0.001+totalAdsorbedNumber * descriptorsDotConc)
    rawString = ",".join([ str(a) for a in descriptorNums]) 
    enrichString = ",".join([ str(a) for a in enrichmentVals]) 
    massEnrichString = ",".join([ str(a) for a in massEnrichmentVals]) 
    print( str(lineTerms[0]) +"," + rawString + "," + enrichString + "," + massEnrichString )
    
    
