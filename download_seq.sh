#! /bin/bash

# Directory containing the PDB files you need the sequences for
DIR="/home/dpower/UnitedAtom/pdbs/itasar"

OUTPUT="seq/itassar"

for PDBPATH in $(find ${DIR} -name "*.pdb")
do
    COL=$(echo ${PDBPATH} | awk -F/ '{printf NF}')
    ROOT=$(echo ${PDBPATH} | awk -v col=${COL} -F/ '{printf $col}')
    FILENAME=$(echo ${ROOT} | awk -F. '{printf $1}')
    #wget "https://www.rcsb.org/pdb/download/downloadFastaFiles.do?structureIdList=${FILENAME}&compressionType=uncompressed" -O "${OUTPUT}/${FILENAME}.fasta"
    wget "https://www.uniprot.org/uniprot/${FILENAME}.fasta" -O "${OUTPUT}/${FILENAME}.fasta"
done


