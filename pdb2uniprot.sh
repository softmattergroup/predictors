#! /bin/bash

for x in $(cat list.txt)
    do
        echo "${x} -> $(curl -s http://www.rcsb.org/pdb/explore/remediatedSequence.do?structureId=${x} | grep '<b>UniProtKB')"
done
