#! /bin/bash

DIR="pepstats"

echo "UniprotID,MolecularWeight,Residues,AverageResidueWeight,Charge,IsoelectricPoint,MolarExtinctionCoefficientsReduced,MolarExtinctionCoefficientsCB,ExtinctionCoefficientsReduced,ExtinctionCoefficientsCB,ProbabilityOfExpressionInInclusionBodies,AlaNumber,AlaMole,AlaDayhoffStat,CysNumber,CyseMole,CysteineDayhoffStat,AspNumber,AspMole,AspDayhoffStat,GluNumber,GluMole,GluDayhoffStat,PheNumber,PheMole,PheDayhoffStat,GlyNumber,GlyMole,GlyDayhoffStat,HisNumber,HisMole,HisDayhoffStat,IleNumber,IleMole,IleDayhoffStat,LysNumber,LysMole,LysDayhoffStat,LeuNumber,LeuMole,LeuDayhoffStat,MetNumber,MetMole,MetDayhoffStat,AsnNumber,AsnMole,AsnDayhoffStat,ProNumber,ProMole,ProDayhoffStat,GlnNumber,GlnMole,GlnDayhoffStat,ArgNumber,ArgMole,ArgDayhoffStat,SerNumber,SerMole,SerDayhoffStat,ThrNumber,ThrMole,ThrDayhoffStat,ValNumber,ValMole,ValDayhoffStat,TrpNumber,TrpMole,TrpDayhoffStat,TyrNumber,TyrMole,TyrDayhoffStat,TinyNumber,TinyMole,SmallNumber,SmallMole,AliphaticNumber,AliphaticMole,AromaticNumber,AromaticMole,NonPolarNumber,NonPolarMole,PolarNumber,PolarMole,ChargedNumber,ChargedMole,BasicNumber,BasicMole,AcidicNumber,AcidicMole"

for FILENAME in $(find ${DIR} -name "*.pepstats")
do
    python3 parse_pepstats.py $FILENAME
done
