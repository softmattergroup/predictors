#! /usr/bin/python

from sys import argv

def GrabOld(line, cols):
    args = filter(None, line.split())
    return [args[i] for i in cols]

def Grab(line,cols):
    lineTerms = line.split()
    return[ lineTerms[i] for i in cols]


filename = argv[1]
root = filename.split('/')[-1]
name = root.split('.')[0]

handle = open(filename, 'r')
lines = handle.read().splitlines()
handle.close()

molWeight, residues             = Grab(lines[2], [3, 6])
avgResWeight, charge            = Grab(lines[3], [4, 7])
isoPoint                        = Grab(lines[4], [3])[0]
molECReduced, molECCystine      = Grab(lines[5], [5, 7])
ECReduced, ECReducedCystine     = Grab(lines[6], [5, 7])

p, inclusionP = Grab(lines[7], [0, 7])
if p == "Improbability":
    inclusionP = str(1.0 - float(inclusionP))

aminoAcids = ['Ala','Arg','Asn','Asp','Cys','Gln','Glu','Gly','His','Ile','Leu','Lys','Met','Phe','Pro','Ser','Thr','Trp','Tyr','Val']
aminoAcidValues = {}

aminoAcidValues[aminoAcids[0]]  = Grab(lines[10], [3, 4, 5])
aminoAcidValues[aminoAcids[1]]  = Grab(lines[27], [3, 4, 5])
aminoAcidValues[aminoAcids[2]]  = Grab(lines[23], [3, 4, 5])
aminoAcidValues[aminoAcids[3]]  = Grab(lines[13], [3, 4, 5])
aminoAcidValues[aminoAcids[4]]  = Grab(lines[12], [3, 4, 5])
aminoAcidValues[aminoAcids[5]]  = Grab(lines[26], [3, 4, 5])
aminoAcidValues[aminoAcids[6]]  = Grab(lines[14], [3, 4, 5])
aminoAcidValues[aminoAcids[7]]  = Grab(lines[16], [3, 4, 5])
aminoAcidValues[aminoAcids[8]]  = Grab(lines[17], [3, 4, 5])
aminoAcidValues[aminoAcids[9]]  = Grab(lines[18], [3, 4, 5])
aminoAcidValues[aminoAcids[10]] = Grab(lines[21], [3, 4, 5])
aminoAcidValues[aminoAcids[11]] = Grab(lines[20], [3, 4, 5])
aminoAcidValues[aminoAcids[12]] = Grab(lines[22], [3, 4, 5])
aminoAcidValues[aminoAcids[13]] = Grab(lines[15], [3, 4, 5])
aminoAcidValues[aminoAcids[14]] = Grab(lines[25], [3, 4, 5])
aminoAcidValues[aminoAcids[15]] = Grab(lines[28], [3, 4, 5])
aminoAcidValues[aminoAcids[16]] = Grab(lines[29], [3, 4, 5])
aminoAcidValues[aminoAcids[17]] = Grab(lines[32], [3, 4, 5])
aminoAcidValues[aminoAcids[18]] = Grab(lines[34], [3, 4, 5])
aminoAcidValues[aminoAcids[19]] = Grab(lines[31], [3, 4, 5])

groups = ['Tiny', 'Small', 'Aliphatic', 'Aromatic', 'Nonpolar', 'Polar', 'Charged', 'Basic', 'Acidic']
groupValues = {}

groupValues[groups[0]] =        Grab(lines[38], [2, 3])
groupValues[groups[1]] =        Grab(lines[39], [2, 3])
groupValues[groups[2]] =        Grab(lines[40], [2, 3])
groupValues[groups[3]] =        Grab(lines[41], [2, 3])
groupValues[groups[4]] =        Grab(lines[42], [2, 3])
groupValues[groups[5]] =        Grab(lines[43], [2, 3])
groupValues[groups[6]] =        Grab(lines[44], [2, 3])
groupValues[groups[7]] =        Grab(lines[45], [2, 3])
groupValues[groups[8]] =        Grab(lines[46], [2, 3])


first = ",".join([molWeight, residues, avgResWeight, charge, isoPoint, molECReduced, molECCystine, ECReduced, ECReducedCystine, inclusionP])

data = [aminoAcidValues[aminoAcid] for aminoAcid in aminoAcids]
data = [ item for value in data for item in value ] # Flatten list
second = ",".join(data)

data = [groupValues[group] for group in groups]
data = [ item for value in data for item in value ] # Flatten list
third = ",".join(data)

print(",".join([name, first, second, third]))
