#include "cg_pdb.h"

#include <iostream>
#include <cmath>

const float deg2rad = M_PI / 180.0;
const int   nrows   = 18;
const int   ncols   = 36;
const float delta   = 5.0;

struct Vector {
    float m_x, m_y, m_z;
    float m_phi, m_theta;

    Vector(float x, float y, float z) : m_x(x), m_y(y), m_z(z) {
        m_phi = std::atan2(m_y, m_x);
        float r = std::sqrt(x*x + y*y);
        m_theta = std::atan2(m_z, r);
    }

    Vector(float phi, float theta) : m_phi(phi), m_theta(theta) {
        m_x = std::sin(theta) * std::cos(phi);
        m_y = std::sin(theta) * std::sin(phi);
        m_z = std::cos(theta);
    }

    float Dot(const Vector& other) const {
        return this->m_x * other.m_x + this->m_y * other.m_y + this->m_z * other.m_z;
    }

    float Length() const {
        return std::sqrt(m_x * m_x + m_y * m_y + m_z * m_z);
    }

};

struct Sphere : public Vector {
    float m_radius;

    Sphere(float x, float y, float z, float radius) : Vector(x, y, z), m_radius(radius) {}

    float Intersect(const Vector& other) const {
        float dot = this->Dot(other);
        float b = std::sqrt(m_radius * m_radius - this->Length() * this->Length() + dot * dot);
        if(std::isnan(b)) {
            return 0.0;
        }
        float distance = std::max(dot + b, dot - b);
        if(distance < 0) {
            return 0.0;
        }
        return distance;
    }
};

float Gini(std::vector<std::vector<float>>& map) {
    const int rows = map.size();
    const int cols = map[0].size();
    float top = 0.0, bottom = 0.0;
    for(int row_i = 0; row_i < rows; ++row_i) {
        for(int col_i = 0; col_i < cols; ++col_i) {
            for(int row_j = 0; row_j < rows; ++row_j) {
                for(int col_j = 0; col_j < cols; ++col_j) {
                    top += std::fabs(map[row_i][col_i] - map[row_j][col_j]);
                }
            }
            bottom += std::fabs(map[row_i][col_i]);
        }
    }
    return top / (2.0 * rows * cols * bottom);
}

int main(int argc, const char* argv[]) {

    for(int arg=1; arg<argc; ++arg) {
        cg_pdb pdb(argv[arg]);

        std::vector<Sphere>                 spheres;
        std::vector<std::string>            names;
        std::vector<std::vector<float>>     map(std::vector<std::vector<float>>(nrows, std::vector<float>(ncols, 0.0)));

        for(int i=0; i<(int)pdb.m_x.size(); ++i) {
            spheres.emplace_back(pdb.m_x[i], pdb.m_y[i], pdb.m_z[i], pdb.m_r[i]);
            names.emplace_back(pdb.m_amino_acid[i]);
        }

        for(int row=0; row<nrows; ++row) {
            for(int col=0; col<ncols; ++col) {

                float phi   = row * delta * deg2rad;            
                float theta = col * delta * deg2rad;            

                Vector unit(phi, theta);

                float max_distance = 0.0;

                for(int i=0; i<(int)spheres.size(); ++i) {

                    float distance = spheres[i].Intersect(unit);

                    if(distance > max_distance) {
                        max_distance = distance;
                        map[row][col] = amino_acid_charges[names[i]];
                    }
                }

                map[row][col] = max_distance;
            }
        }

        std::cout << Gini(map); 
    }

    return 0;
}
