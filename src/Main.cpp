#include "CoarseGrainedPDB.h"

void help() {
    std::cout << "Usage: ./Predictors3D <--list list.txt> [--dir .] [--help]\n";
    std::exit(EXIT_FAILURE);
}

int main (const int argc, const char * argv[]) {

    std::string myList;
    std::string myDir = ".";

    for (int i = 1; i < argc; ++i) {
        if (std::string(argv[i]) == "--list")
            myList = std::string(argv[++i]);
        else if (std::string(argv[i]) == "--dir")
            myDir = std::string(argv[++i]);
        else
            help();
    }

    if (myList.empty()) 
        help(); 

    std::ifstream handle(myList.c_str());
    
    if (!handle.is_open()) {
        std::cout << "Error: Could not open file '" << myList << "'\n";
        return EXIT_FAILURE;
    }
    
    std::string line;

    std::cout << CoarseGrainedPDB::print_header() << "\n";

    while (std::getline (handle, line))
    {
        std::string path = myDir + '/' + line;
        CoarseGrainedPDB pdb (path);
        std::cout << pdb.print_predictors() << "\n";
    }

    handle.close();

    return EXIT_SUCCESS;
}
