#include "cgpdb.h"

#include <cmath>

inline bool is_within(const double x1, const double y1, const double z1, const double x2, const double y2, const double z2, const double r) {
    if((x2-x1)*(x2-x1) + (y2-y1)*(y2-y1) + (z2-z1)*(z2-z1) < r*r) {
        return true;
    }
    return false;
}

constexpr double buffer = 1.0;

class Volume : public CoarseGrainedPDB
{
    private:
        const std::size_t   m_steps;
        const double        m_buffer;

    public:
        Volume(const std::string& filename, const std::size_t steps = 1e9, const double buffer = 0.3)
            : CoarseGrainedPDB(filename)
            , m_steps(steps)
            , m_buffer(buffer)
        {}

        void calculate_volume() const {
            
            constexpr std::size_t   steps       = 1000000000;
            constexpr double        buffer      = 0.3;
            const BoxLimits         boxLimits   = get_box_limits();
            const double            boxVolume   = calcule_box_volume (boxLimits);
            const double            dV          = box_volume / N;
            const double            stepSize    = std::pow(dV, 1.0 / 3.0);

            std::size_t n = 0;

            for (double x = boxLimits[0].first - buffer; x < boxLimits[0].second + buffer; x += stepSize)
            {
                for (double y = boxLimits[1].first - buffer; y < boxLimits[1].second + buffer; y += stepSize)
                {
                    for (double z = boxLimits[2].first - buffer; z < boxLimits[2].second + buffer; z += stepSize)
                    {
                        for (std::size_t i = 0; i < m_x.size(); ++i)
                        {
                            if (is_point_within_i (x, y, z, i)) {
                                ++i;
                                break;
                            }
                        }
                        // end i
                    }
                    // end z
                }
                // end y
            }
            // end x
        
            return (n / static_cast<double>(N)) * boxVolume; 
        }
};


int main(int argc, const char* argv[]) {
    if(argc < 2) {
        std::cout << "Usage: ./volume <file.pdb> [file.pdb] ..." << std::endl;
        std::cout << std::endl;
        std::cout << "  They output will be a list of volumes in units of nm^3" << std::endl;
        return -1;
    }
    for(int i=1; i<argc; ++i) {
        Volume volume(argv[i]);
        std::cout << volume.m_pdb_volume << std::endl;
    }
    
    return 0;
}
