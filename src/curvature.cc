#include "cg_pdb.h"

#include <iostream>
#include <cmath>

const double deg2rad = M_PI / 180.0;
const int   nrows   = 18;
const int   ncols   = 36;
const double delta   = 5.0;

struct Vector {
    double m_x, m_y, m_z;
    double m_phi, m_theta;

    Vector(double x, double y, double z) : m_x(x), m_y(y), m_z(z) {
        m_phi = std::atan2(m_y, m_x);
        double r = std::sqrt(x*x + y*y);
        m_theta = std::atan2(m_z, r);
    }

    Vector(double phi, double theta) : m_phi(phi), m_theta(theta) {
        m_x = std::sin(theta) * std::cos(phi);
        m_y = std::sin(theta) * std::sin(phi);
        m_z = std::cos(theta);
    }

    double Dot(const Vector& other) const {
        return this->m_x * other.m_x + this->m_y * other.m_y + this->m_z * other.m_z;
    }

    double Length() const {
        return std::sqrt(m_x * m_x + m_y * m_y + m_z * m_z);
    }

};

struct Sphere : public Vector {
    double m_radius;

    Sphere(double x, double y, double z, double radius) : Vector(x, y, z), m_radius(radius) {}

    double Intersect(const Vector& other) const {
        double dot = this->Dot(other);
        double b = std::sqrt(m_radius * m_radius - this->Length() * this->Length() + dot * dot);
        if(std::isnan(b)) {
            return 0.0;
        }
        double distance = std::max(dot + b, dot - b);
        if(distance < 0) {
            return 0.0;
        }
        return distance;
    }
};

int Row(int row) {
    if(row < 0) {
        return nrows + row;
    }
    else if(row >= nrows) {
        return row - nrows;
    }
    else {
        return row;
    }
}

int Col(int col) {
    if(col < 0) {
        return ncols + col;
    }
    else if(col >= ncols) {
        return col - ncols;
    }
    else {
        return col;
    }
}

double Gini(std::vector<std::vector<double>>& map) {
    const int rows = map.size();
    const int cols = map[0].size();
    double top = 0.0, bottom = 0.0;
    for(int row_i = 0; row_i < rows; ++row_i) {
        for(int col_i = 0; col_i < cols; ++col_i) {
            for(int row_j = 0; row_j < rows; ++row_j) {
                for(int col_j = 0; col_j < cols; ++col_j) {
                    top += std::fabs(map[row_i][col_i] - map[row_j][col_j]);
                }
            }
            bottom += std::fabs(map[row_i][col_i]);
        }
    }
    return top / (2.0 * rows * cols * bottom);
}

int main(int argc, const char* argv[]) {

    for(int arg=1; arg<argc; ++arg) {
        cg_pdb pdb(argv[arg]);

        std::vector<Sphere>                 spheres;
        std::vector<std::vector<double>>     map(std::vector<std::vector<double>>(nrows, std::vector<double>(ncols, 0.0)));

        for(int i=0; i<(int)pdb.m_x.size(); ++i) {
            spheres.emplace_back(pdb.m_x[i], pdb.m_y[i], pdb.m_z[i], pdb.m_r[i]);
        }

        for(int row=0; row<nrows; ++row) {
            for(int col=0; col<ncols; ++col) {

                double phi   = row * delta * deg2rad;            
                double theta = col * delta * deg2rad;            

                Vector unit(phi, theta);

                double max_distance = 0.0;

                for(int i=0; i<(int)spheres.size(); ++i) {

                    double distance = spheres[i].Intersect(unit);

                    if(distance > max_distance) {
                        max_distance = distance;
                    }
                }

                map[row][col] = max_distance;
            }
        }

        std::vector<std::vector<double>>     deriv_map(std::vector<std::vector<double>>(nrows, std::vector<double>(ncols, 0.0)));
        
        for(int row=0; row<nrows; ++row) {
            for(int col=0; col<ncols; ++col) {

                double average = (
                    map[Row(row - 1)][Col(col - 1)] +
                    map[Row(row - 1)][Col(col)] +
                    map[Row(row - 1)][Col(col + 1)] +
                    map[Row(row)][Col(col - 1)] +
                    map[Row(row)][Col(col + 1)] +
                    map[Row(row + 1)][Col(col - 1)] +
                    map[Row(row + 1)][Col(col)] +
                    map[Row(row + 1)][Col(col + 1)]) / 8.0;

                deriv_map[row][col] = average - map[row][col];
    
                //std::cout << deriv_map[row][col] << (col == ncols - 1 ? '\n' : ',');
            }
        }

        std::cout << Gini(deriv_map);
    }

    return 0;
}
