// Run the following to install the necessery linear algebra c++ libraries
//
//  sudo apt-get install libeigen3-dev
//
// Compile command
//
//  g++ -I/usr/include/eigen3 eigen.cc

#include <eigen3/Eigen/Dense> // Eigen::MatrixXd
#include <eigen3/Eigen/Eigenvalues> // Eigen::EigenSolver

typedef Eigen::Matrix<float, 3, 3> Matrix;
typedef Eigen::Matrix<float, 3, 1> Vector;

#include "cg_pdb.h"

#include <vector>
#include <string>
#include <iostream>

struct Inertia : public cg_pdb {
    float m_inertia_x;
    float m_inertia_y;
    float m_inertia_z;

    Inertia(const std::string& filename) : cg_pdb(filename) {
        Matrix matrix = Matrix::Zero(3, 3);
		for(unsigned int i=0; i<m_x.size(); ++i) {
    		matrix(0, 0) += m_mass[i] * (m_y[i] * m_y[i] + m_z[i] * m_z[i]);
    		matrix(0, 1) -= m_mass[i] * m_x[i] * m_y[i];
    		matrix(0, 2) -= m_mass[i] * m_x[i] * m_z[i];
    		matrix(1, 1) += m_mass[i] * (m_z[i] * m_z[i] + m_x[i] * m_x[i]);
    		matrix(1, 2) -= m_mass[i] * m_y[i] * m_z[i];
    		matrix(2, 2) += m_mass[i] * (m_x[i] * m_x[i] + m_y[i] * m_y[i]);
		}	
		matrix(1, 0) = matrix(0, 1);
		matrix(2, 0) = matrix(0, 2);
		matrix(2, 1) = matrix(1, 2);
    	Eigen::EigenSolver<Matrix> eigen_solver(matrix);
        Vector inertias = eigen_solver.eigenvalues().real();
        m_inertia_x = inertias(0);
        m_inertia_y = inertias(1);
        m_inertia_z = inertias(2);
	}
};

int main(int argc, const char* argv[]) {
    if(argc < 2) {
        std::cout << "Usage: ./inertia <file.pdb> [file.pdb] .." << std::endl;
        std::cout << std::endl;
        std::cout << "  Returns a csv list of principles moments (x, y, z) of inertia in units of daltons * nm^2" << std::endl;
    }
    for(int i=1; i<argc; ++i) {
        Inertia inertia(argv[i]);
        std::cout << inertia.m_inertia_x << "," << inertia.m_inertia_y << "," << inertia.m_inertia_z << std::endl;
    }
    return 0;
}
