#include "cg_pdb.h"

#include <cmath>
#include <vector>
#include <iostream>

#define GOLDEN_RATIO 1.61803398875

inline bool is_within(const float x1, const float y1, const float z1, const float x2, const float y2, const float z2, const float r) {
    if((x2-x1)*(x2-x1) + (y2-y1)*(y2-y1) + (z2-z1)*(z2-z1) < r*r) {
        return true;
    }
    return false;
}

struct Sphere {
    float m_c_x, m_c_y, m_c_z, m_r;
    int m_n;
    std::vector<float> m_x, m_y, m_z;
    std::vector<bool> m_surface;

    Sphere(float x, float y, float z, float r, int n)
        : m_c_x(x)
        , m_c_y(y)
        , m_c_z(z)
        , m_r(r)
        , m_n(n)
        , m_x(std::vector<float>(n))
        , m_y(std::vector<float>(n))
        , m_z(std::vector<float>(n))
        , m_surface(std::vector<bool>(n, true))
    {
        for(int i=0; i<m_n; ++i) {
            float theta = i * GOLDEN_RATIO;
            float phi = std::acos(1 - (2.0 * i / n) - (1.0 / n));
            m_x[i] = (m_c_x + m_r * std::sin(phi) * std::cos(theta));
            m_y[i] = (m_c_y + m_r * std::sin(phi) * std::sin(theta));
            m_z[i] = (m_c_z + m_r * std::cos(phi));
        }
    }

    void chomp(const Sphere& other) {
        for(int i=0; i<(int)m_x.size(); ++i) {
            if(!m_surface[i]) {
                continue;
            }
            if(is_within(m_x[i], m_y[i], m_z[i], other.m_c_x, other.m_c_y, other.m_c_z, other.m_r)) {
                m_surface[i] = false;
            }
        }
    }

    float area() const {
        int n = 0;
        for(int i=0; i<(int)m_x.size(); ++i) {
            n += m_surface[i];
        }
        return (n / (float) m_n) * 4 * M_PI * m_r * m_r; 
    }
};

struct SurfaceArea : public cg_pdb {
    float m_pdb_surface_area;
    std::vector<Sphere> m_spheres;

    SurfaceArea(const std::string& filename, int N = 100) : cg_pdb(filename) {
        for(int i=0; i<(int)m_x.size(); ++i) {
            m_spheres.emplace_back(m_x[i], m_y[i], m_z[i], m_r[i], N);
        }
        for(int i=0; i<(int)m_x.size(); ++i) {
            for(int j=0; j<(int)m_x.size(); ++j) {
                if(i <= j) {
                    continue;
                }
                m_spheres[i].chomp(m_spheres[j]);
            }
        }
        for(int i=0; i<(int)m_x.size(); ++i) {
            m_pdb_surface_area += m_spheres[i].area();
        }
    }
};

int main(int argc, const char* argv[]) {
    if(argc < 2) {
        std::cout << "Usage: ./surfacearea <file.pdb> [file.pdb] ..." << std::endl;
        std::cout << std::endl;
        std::cout << "  Returns a list of protein surface areas in units of nm^2" << std::endl;
        return -1;
    }
    for(int i=1; i<argc; ++i) {
        SurfaceArea surfacearea(argv[i]);
        std::cout << surfacearea.m_pdb_surface_area << std::endl;
    }
    return 0;
}
