#ifndef COARSE_GRAINED_PDB__H__
#define COARSE_GRAINED_PDB__H__

#include <list>
#include <array>
#include <cmath>
#include <string>
#include <vector>
#include <fstream>
#include <numeric>
#include <iostream>
#include <algorithm>
#include <string_view>
#include <unordered_map>
#include <eigen3/Eigen/Dense>
#include <eigen3/Eigen/Eigenvalues>

using MolMap    = const std::unordered_map<std::string, double>;
using BoxLimits = std::array<std::pair<double, double>, 3>;
using Point     = std::array<double, 3>;
using Matrix    = Eigen::Matrix<double, 3, 3>;
using Vector    = Eigen::Matrix<double, 3, 1>;
using Rotation  = std::array<std::array<double, 3>, 3>;

constexpr double GOLDEN_RATIO = 1.61803398875;

MolMap AMINO_ACID_RADII {
	{ "ALA", 3.23 }, { "ARG", 4.29 }, { "ASN", 3.62 }, { "ASP", 3.56 }, { "CYS", 3.52 }, 
	{ "GLN", 3.86 }, { "GLU", 3.76 }, { "GLY", 2.85 }, { "HIS", 3.02 }, { "ILE", 4.01 }, 
	{ "LEU", 4.01 }, { "LYS", 4.05 }, { "MET", 4.01 }, { "PHE", 4.21 }, { "PRO", 3.63 }, 
	{ "SER", 3.28 }, { "THR", 3.57 }, { "TRP", 4.49 }, { "TYR", 4.25 }, { "VAL", 3.78 }
};

MolMap AMINO_ACID_MASSES {
	{ "ALA",  71.08 }, { "ARG", 156.19 }, { "ASN", 114.11 }, { "ASP", 115.09 }, { "CYS", 103.15 },
	{ "GLN", 128.13 }, { "GLU", 129.12 }, { "GLY",  57.05 }, { "HIS", 137.14 }, { "ILE", 113.16 },
	{ "LEU", 113.16 }, { "LYS", 128.18 }, { "MET", 131.20 }, { "PHE", 147.18 }, { "PRO",  97.12 },
	{ "SER",  87.08 }, { "THR", 101.11 }, { "TRP", 186.22 }, { "TYR", 163.18 }, { "VAL",  99.13 } 
};

MolMap AMINO_ACID_CHARGES {
	{ "ALA",  0.0 }, { "ARG",  1.0 }, { "ASN",  0.0 }, { "ASP", -1.0 }, { "CYS",  0.0 },
	{ "GLN",  0.0 }, { "GLU", -1.0 }, { "GLY",  0.0 }, { "HIS",  1.0 }, { "ILE",  0.0 },
	{ "LEU",  0.0 }, { "LYS",  1.0 }, { "MET",  0.0 }, { "PHE",  0.0 }, { "PRO",  0.0 },
	{ "SER",  0.0 }, { "THR",  0.0 }, { "TRP",  0.0 }, { "TYR",  0.0 }, { "VAL",  0.0 }
};

MolMap DAYHOFF_STATISTICS {
	{ "ALA", 8.6 }, { "ARG", 4.9 }, { "ASN", 4.3 }, { "ASP", 5.5 }, { "CYS", 2.9 },
	{ "GLN", 3.9 }, { "GLU", 6.0 }, { "GLY", 8.4 }, { "HIS", 2.0 }, { "ILE", 4.5 },
	{ "LEU", 7.4 }, { "LYS", 6.6 }, { "MET", 1.7 }, { "PHE", 3.6 }, { "PRO", 5.2 },
	{ "SER", 7.0 }, { "THR", 6.1 }, { "TRP", 1.3 }, { "TYR", 3.4 }, { "VAL", 6.6 }
};

std::pair<double, double> operator + (const std::pair<double, double>& lhs, const std::pair<double, double>& rhs) {
    return { lhs.first + rhs.first, lhs.second + rhs.second };
}

class CoarseGrainedPDB
{
    private:
	    std::vector<double>         m_x;
        std::vector<double>         m_y;
        std::vector<double>         m_z;
        std::vector<std::string>    m_labels;
        std::vector<double>         m_radii;
        std::vector<double>         m_masses;
        std::vector<double>         m_charges;
        std::vector<double>         m_surfaceFraction;
        Rotation                    m_rotation;

    public:
        CoarseGrainedPDB (const std::string& filename)
        {
            read_file(filename);
            move_to_center_of_mass();
        }

    private:
//-------------------- FILE IO --------------------
        
        void read_file (const std::string& filename) {
		    std::ifstream handle(filename.c_str());
		    
            if(!handle.is_open()) {
		        std::cout << "Error: Failed to find file '" << filename << "'\n";
                std::exit(EXIT_FAILURE);
            }

      	    std::string line;
      	    
            while(std::getline(handle, line)) {
			
          	    if(std::string_view(line).substr(0, 4) == "ATOM" && std::string_view(line).substr(13, 2) == "CA") {
                    
                    double x, y, z, radius, mass, charge;	
                    std::string label;

                    try {
                        x       = std::stod(line.substr(30, 8));
          		        y       = std::stod(line.substr(38, 8));
              	        z       = std::stod(line.substr(46, 8));
                        label   = line.substr(17, 3);
                        radius  = AMINO_ACID_RADII.at(label);
              	        mass    = AMINO_ACID_MASSES.at(label);
                        charge  = AMINO_ACID_CHARGES.at(label);
                    }
                    catch (const std::invalid_argument& ia) {
                        std::cout << "Error: Failed to parse file '" << filename << "'\n";
                        std::cout << "Error: " << line << "\n";
                        std::exit(EXIT_FAILURE);
                    }
                    
                    m_x.emplace_back(x);
                    m_y.emplace_back(y);
                    m_z.emplace_back(z);
                    m_labels.emplace_back(label);
                    m_radii.emplace_back(radius);
                    m_masses.emplace_back(mass);
                    m_charges.emplace_back(charge);
                    m_surfaceFraction.emplace_back(0.0);
                }
		    }
    	   
            handle.close();
        }


//-------------------- MASS --------------------
        
        void move_to_center_of_mass() { 
            const double totalMass = calculate_mass();

            const double x0 = std::inner_product (std::cbegin(m_x), std::cend(m_x), std::cbegin(m_masses), 0.0) / totalMass;
            const double y0 = std::inner_product (std::cbegin(m_y), std::cend(m_y), std::cbegin(m_masses), 0.0) / totalMass;
            const double z0 = std::inner_product (std::cbegin(m_z), std::cend(m_z), std::cbegin(m_masses), 0.0) / totalMass;
       
            std::transform (std::cbegin(m_x), std::cend(m_x), std::begin(m_x), [x0] (double x) { return x - x0; });
            std::transform (std::cbegin(m_y), std::cend(m_y), std::begin(m_y), [y0] (double y) { return y - y0; });
            std::transform (std::cbegin(m_z), std::cend(m_z), std::begin(m_z), [z0] (double z) { return z - z0; });
        }

        double calculate_mass() const {
            return std::accumulate (std::cbegin(m_masses), std::cend(m_masses), 0.0);
        }

//-------------------- VOLUME --------------------
        
        std::pair<double, double> get_min_max_values (const std::vector<double>& vec) const {
            return {
                *std::min_element (std::cbegin(vec), std::cend(vec)),
                *std::max_element (std::cbegin(vec), std::cend(vec))
            };
        }

        BoxLimits get_box_limits() const {
            return {
                get_min_max_values (m_x),
                get_min_max_values (m_y),
                get_min_max_values (m_z)
            };
        }

        double calculate_box_volume (const BoxLimits& boxLimits) const {
            return (boxLimits[0].second - boxLimits[0].first) * (boxLimits[1].second - boxLimits[1].first) * (boxLimits[2].second - boxLimits[2].first);
        }

        bool is_point_within_i (const double x, const double y, const double z, const std::size_t i) const noexcept {
            return (x - m_x[i]) * (x - m_x[i]) + (y - m_y[i]) * (y - m_y[i]) + (z - m_z[i]) * (z - m_z[i]) < m_radii[i] * m_radii[i];
        }

      	double calculate_volume() const {
      		constexpr std::size_t   steps       = 1000000;
            constexpr double        buffer      = 0.3;
            const BoxLimits         boxLimits   = get_box_limits();
            const double            boxVolume   = calculate_box_volume (boxLimits);
            const double            dV          = boxVolume / steps;
            const double            stepSize    = std::pow(dV, 1.0 / 3.0);
 
            std::size_t n = 0;
 
            for (double x = boxLimits[0].first - buffer; x < boxLimits[0].second + buffer; x += stepSize)
            {   
                for (double y = boxLimits[1].first - buffer; y < boxLimits[1].second + buffer; y += stepSize)
                {   
                    for (double z = boxLimits[2].first - buffer; z < boxLimits[2].second + buffer; z += stepSize)
                    {
                      	for (std::size_t i = 0; i < m_x.size(); ++i)
                        {
                          	if (is_point_within_i (x, y, z, i)) {
                            	++n;
                             	break;
                          	}
                      	}
                      	// end i
                  	}
                  	// end z
              	}
              	// end y
         	}
          	// end x
 
          	return (n / static_cast<double>(steps)) * boxVolume; 
     	}

//-------------------- SURFACE AREA --------------------
   
        std::list<Point> create_surface_points (const std::size_t sphere, const std::size_t npoints) const {
            std::list<Point> points;

            for (std::size_t point = 0; point < npoints; ++point)
            {
                double theta        = point * GOLDEN_RATIO;
                double phi          = std::acos(1 - (2.0 * point / static_cast<double>(npoints)) - (1.0 / npoints));
                
                points.push_back({
                    m_x[sphere] + m_radii[sphere] * std::sin(phi) * std::cos(theta),
                    m_y[sphere] + m_radii[sphere] * std::sin(phi) * std::sin(theta),
                    m_z[sphere] + m_radii[sphere] * std::cos(phi)
                });
            }
        
            return points;
        }

        bool is_point_within_sphere (const Point& point, const std::size_t index) const noexcept {
            return (m_x[index] - point[0]) * (m_x[index] - point[0]) + (m_y[index] - point[1]) * (m_y[index] - point[1]) + (m_z[index] - point[2]) * (m_z[index] - point[2]) < m_radii[index] * m_radii[index];
        }

        double calculate_surface_area() {
            constexpr std::size_t           npoints = 1000;
            std::vector<std::list<Point>>   spheres (m_x.size());

            for (std::size_t sphere = 0; sphere < m_x.size(); ++sphere)
                spheres[sphere] = create_surface_points (sphere, npoints);

            for (std::size_t first = 0; first < m_x.size(); ++first)
            {
                for (std::size_t second = 0; second < m_x.size(); ++second)
                {
                    if (first == second)
                        continue;

                    for (auto point = std::cbegin(spheres[first]); point != std::cend(spheres[first]);)
                        point = is_point_within_sphere (*point, second) ? spheres[first].erase(point) : ++point;
                }
            }

            double surfaceArea = 0.;
            
            for (std::size_t i = 0; i < m_x.size(); ++i)
            {
                double fraction      = spheres[i].size() / static_cast<double>(npoints);
                m_surfaceFraction[i] = fraction;
                surfaceArea          += 4.0 * M_PI * m_radii[i] * m_radii[i] * fraction;
            }

            return surfaceArea;
        }


        double calculate_sphericity (const double volume, const double area) const {
            return std::pow(M_PI, 1./3.) * std::pow(6 * volume, 2./3.) / area;
        }

//-------------------- SURFACE AMINO ACIDS --------------------

        double number_of_surface_amino_acids () const {
            return std::accumulate (std::cbegin(m_surfaceFraction), std::cend(m_surfaceFraction), 0.);
        }

        double number_of_specific_amino_acid_on_surface (const std::string& label) const {
            double count = 0;
            for (std::size_t i = 0; i < m_x.size(); ++i) {
                if (m_labels[i] == label)
                    count += m_surfaceFraction[i];
            }
            return count;
        }

        std::pair<double, double> surface_amino_acid_cp(const std::string& label, const double total) const {
            const double count      = number_of_specific_amino_acid_on_surface(label);
            const double percentage = count * 100.0 / total;
            return { count, percentage };
        }

		double dayHoffStat (const std::string label, const double percentage) const {
			return percentage / DAYHOFF_STATISTICS.at(label);
		}

//-------------------- MOMENTS OF INERTIA --------------------

        Point calculate_moments_of_inertia() {

            Matrix matrix = Matrix::Zero(3, 3);

            for(std::size_t  i = 0; i < m_x.size(); ++i)
            {
                matrix(0, 0) += m_masses[i] * (m_y[i] * m_y[i] + m_z[i] * m_z[i]);
                matrix(0, 1) -= m_masses[i] * m_x[i] * m_y[i];
                matrix(0, 2) -= m_masses[i] * m_x[i] * m_z[i];
                matrix(1, 1) += m_masses[i] * (m_z[i] * m_z[i] + m_x[i] * m_x[i]);
                matrix(1, 2) -= m_masses[i] * m_y[i] * m_z[i];
                matrix(2, 2) += m_masses[i] * (m_x[i] * m_x[i] + m_y[i] * m_y[i]);
            }

            matrix(1, 0) = matrix(0, 1);
            matrix(2, 0) = matrix(0, 2);
            matrix(2, 1) = matrix(1, 2);
           
            Eigen::EigenSolver<Matrix> eigen_solver(matrix);
           
            Vector inertias = eigen_solver.eigenvalues().real();
            
            Matrix rotation = eigen_solver.eigenvectors().real();

            for (std::size_t i = 0; i < m_x.size(); ++i)
            {
                Vector X { m_x[i], m_y[i], m_z[i] };
                X = rotation * X; //TODO: Confirm this is the correct operation
                m_x[i] = X(0);
                m_y[i] = X(1);
                m_z[i] = X(2);
            }

            return { inertias(0), inertias(1), inertias(2) };    
        } 
    
//-------------------- ELECTRIC DIPOLE MOMENT --------------------

    Point calculate_dipole_moments() {
        return {
            std::inner_product (std::cbegin(m_x), std::cend(m_x), std::cbegin(m_charges), 0.),
            std::inner_product (std::cbegin(m_y), std::cend(m_y), std::cbegin(m_charges), 0.),
            std::inner_product (std::cbegin(m_z), std::cend(m_z), std::cbegin(m_charges), 0.)
        };
    }

    public:
//------------------- REPORTING RESULTS --------------------
        static std::string print_header(const std::string& delimiter = ", ") {
            return "Mass (Da)"
                + delimiter + "Surface Area (A2)"
                + delimiter + "Volume (A3)"
                + delimiter + "Sphericity"
                + delimiter + "First Principle Moment Of Inertia (Da A2)"
                + delimiter + "Second Principle Moment Of Inertia (Da A2)"
                + delimiter + "Third Principle Moment Of Inertia (Da A2)"
                + delimiter + "First Dipole Moment (Z A)"
                + delimiter + "Second Dipole Moment (Z A)"
                + delimiter + "Third Dipole Moment (Z A)"
                + delimiter + "Surface Amino Acids"
                + delimiter + "Surface Amino Acids %"
                + delimiter + "Surface ALA"
                + delimiter + "Surface ALA %"
                + delimiter + "Surface ALA Dayhoff %"
                + delimiter + "Surface ARG"
                + delimiter + "Surface ARG %"
                + delimiter + "Surface ARG Dayhoff %"
                + delimiter + "Surface ASN"
                + delimiter + "Surface ASN %"
                + delimiter + "Surface ASN Dayhoff %"
                + delimiter + "Surface ASP"
                + delimiter + "Surface ASP %"
                + delimiter + "Surface ASP Dayhoff %"
                + delimiter + "Surface CYS"
                + delimiter + "Surface CYS %"
                + delimiter + "Surface CYS Dayhoff %"
                + delimiter + "Surface GLN"
                + delimiter + "Surface GLN %"
                + delimiter + "Surface GLN Dayhoff %"
                + delimiter + "Surface GLU"
                + delimiter + "Surface GLU %"
                + delimiter + "Surface GLU Dayhoff %"
                + delimiter + "Surface GLY"
                + delimiter + "Surface GLY %"
                + delimiter + "Surface GLY Dayhoff %"
                + delimiter + "Surface HIS"
                + delimiter + "Surface HIS %"
                + delimiter + "Surface HIS Dayhoff %"
                + delimiter + "Surface ILE"
                + delimiter + "Surface ILE %"
                + delimiter + "Surface ILE Dayhoff %"
                + delimiter + "Surface LEU"
                + delimiter + "Surface LEU %"
                + delimiter + "Surface LEU Dayhoff %"
                + delimiter + "Surface LYS"
                + delimiter + "Surface LYS %"
                + delimiter + "Surface LYS Dayhoff %"
                + delimiter + "Surface MET"
                + delimiter + "Surface MET %"
                + delimiter + "Surface MET Dayhoff %"
                + delimiter + "Surface PHE"
                + delimiter + "Surface PHE %"
                + delimiter + "Surface PHE Dayhoff %"
                + delimiter + "Surface PRO"
                + delimiter + "Surface PRO %"
                + delimiter + "Surface PRO Dayhoff %"
                + delimiter + "Surface SER"
                + delimiter + "Surface SER %"
                + delimiter + "Surface SER Dayhoff %"
                + delimiter + "Surface THR"
                + delimiter + "Surface THR %"
                + delimiter + "Surface THR Dayhoff %"
                + delimiter + "Surface TRP"
                + delimiter + "Surface TRP %"
                + delimiter + "Surface TRP Dayhoff %"
                + delimiter + "Surface TYR"
                + delimiter + "Surface TYR %"
                + delimiter + "Surface TYR Dayhoff %"
                + delimiter + "Surface VAL"
                + delimiter + "Surface VAL %"
                + delimiter + "Surface VAL Dayhoff %"
                + delimiter + "Surface Tiny"
                + delimiter + "Surface Tiny %"
                + delimiter + "Surface Small"
                + delimiter + "Surface Small %"
                + delimiter + "Surface Aliphatic"
                + delimiter + "Surface Aliphatic %"
                + delimiter + "Surface Aromatic"
                + delimiter + "Surface Aromatic %"
                + delimiter + "Surface Non-Polar"
                + delimiter + "Surface Non-Polar %"
                + delimiter + "Surface Polar"
                + delimiter + "Surface Polar %"
                + delimiter + "Surface Charged"
                + delimiter + "Surface Charged %"
                + delimiter + "Surface Basic"
                + delimiter + "Surface Basic %"
                + delimiter + "Surface Acidic"
                + delimiter + "Surface Acidic %";
        }

        std::string print_predictors(const std::string& delimiter = ", ") {
            double mass         = calculate_mass();
            double surfaceArea  = calculate_surface_area();
            double volume       = calculate_volume();
            double sphericity   = calculate_sphericity(volume, surfaceArea);
            
            Point  inertia      = calculate_moments_of_inertia();
            Point  dipole       = calculate_dipole_moments();
            
            double surfaceCount           = number_of_surface_amino_acids();           
            double surfaceCountPercentage = surfaceCount * 100.0 / m_x.size();

            auto ala = surface_amino_acid_cp("ALA", surfaceCount);
            auto arg = surface_amino_acid_cp("ARG", surfaceCount);
            auto asn = surface_amino_acid_cp("ASN", surfaceCount);
            auto asp = surface_amino_acid_cp("ASP", surfaceCount);
            auto cys = surface_amino_acid_cp("CYS", surfaceCount);
            auto gln = surface_amino_acid_cp("GLN", surfaceCount);
            auto glu = surface_amino_acid_cp("GLU", surfaceCount);
            auto gly = surface_amino_acid_cp("GLY", surfaceCount);
            auto his = surface_amino_acid_cp("HIS", surfaceCount);
            auto ile = surface_amino_acid_cp("ILE", surfaceCount);
            auto leu = surface_amino_acid_cp("LEU", surfaceCount);
            auto lys = surface_amino_acid_cp("LYS", surfaceCount);
            auto met = surface_amino_acid_cp("MET", surfaceCount);
            auto phe = surface_amino_acid_cp("PHE", surfaceCount);
            auto pro = surface_amino_acid_cp("PRO", surfaceCount);
            auto ser = surface_amino_acid_cp("SER", surfaceCount);
            auto thr = surface_amino_acid_cp("THR", surfaceCount);
            auto trp = surface_amino_acid_cp("TRP", surfaceCount);
            auto tyr = surface_amino_acid_cp("TYR", surfaceCount);
            auto val = surface_amino_acid_cp("VAL", surfaceCount);


			auto dala = dayHoffStat ("ALA", ala.second);
			auto darg = dayHoffStat ("ARG", arg.second);
			auto dasn = dayHoffStat ("ASN", asn.second);
			auto dasp = dayHoffStat ("ASP", asp.second);
			auto dcys = dayHoffStat ("CYS", cys.second);
			auto dgln = dayHoffStat ("GLN", gln.second);
			auto dglu = dayHoffStat ("GLU", glu.second);
			auto dgly = dayHoffStat ("GLY", gly.second);
			auto dhis = dayHoffStat ("HIS", his.second);
			auto dile = dayHoffStat ("ILE", ile.second);
			auto dleu = dayHoffStat ("LEU", leu.second);
			auto dlys = dayHoffStat ("LYS", lys.second);
			auto dmet = dayHoffStat ("MET", met.second);
			auto dphe = dayHoffStat ("PHE", phe.second);
			auto dpro = dayHoffStat ("PRO", pro.second);
			auto dser = dayHoffStat ("SER", ser.second);
			auto dthr = dayHoffStat ("THR", thr.second);
			auto dtrp = dayHoffStat ("TRP", trp.second);
			auto dtyr = dayHoffStat ("TYR", tyr.second);
			auto dval = dayHoffStat ("VAL", val.second);

            auto tiny       = ala + cys + gly + ser + thr;
            auto small      = ala + cys + asp + gly + asn + pro + ser + thr + val;  
            auto aliphatic  = ala + ile + leu + val;
            auto aromatic   = phe + his + trp + tyr;
            auto nonpolar   = ala + cys + phe + gly + ile + leu + met + pro + val + trp + tyr;
            auto polar      = asp + glu + his + lys + asn + gln + arg + ser + thr;
            auto charged    = asp + glu + his + lys + arg;
            auto basic      = his + lys + arg;
            auto acidic     = asp + glu;

            return std::to_string(mass)
                + delimiter + std::to_string(surfaceArea)
                + delimiter + std::to_string(volume)
                + delimiter + std::to_string(sphericity)
                + delimiter + std::to_string(inertia[0])
                + delimiter + std::to_string(inertia[1])
                + delimiter + std::to_string(inertia[2])
                + delimiter + std::to_string(dipole[0])
                + delimiter + std::to_string(dipole[1])
                + delimiter + std::to_string(dipole[2])
                + delimiter + std::to_string(surfaceCount)
                + delimiter + std::to_string(surfaceCountPercentage)
                + delimiter + std::to_string(ala.first)
                + delimiter + std::to_string(ala.second)
				+ delimiter + std::to_string(dala) 
                + delimiter + std::to_string(arg.first)
                + delimiter + std::to_string(arg.second) 
				+ delimiter + std::to_string(darg) 
                + delimiter + std::to_string(asn.first)
                + delimiter + std::to_string(asn.second) 
				+ delimiter + std::to_string(dasn) 
                + delimiter + std::to_string(asp.first)
                + delimiter + std::to_string(asp.second) 
				+ delimiter + std::to_string(dasp) 
                + delimiter + std::to_string(cys.first)
                + delimiter + std::to_string(cys.second) 
				+ delimiter + std::to_string(dcys) 
                + delimiter + std::to_string(gln.first)
                + delimiter + std::to_string(gln.second) 
				+ delimiter + std::to_string(dgln) 
                + delimiter + std::to_string(glu.first)
                + delimiter + std::to_string(glu.second) 
				+ delimiter + std::to_string(dglu) 
                + delimiter + std::to_string(gly.first)
                + delimiter + std::to_string(gly.second) 
				+ delimiter + std::to_string(dgly) 
                + delimiter + std::to_string(his.first)
                + delimiter + std::to_string(his.second) 
				+ delimiter + std::to_string(dhis) 
                + delimiter + std::to_string(ile.first)
                + delimiter + std::to_string(ile.second) 
				+ delimiter + std::to_string(dile) 
                + delimiter + std::to_string(leu.first)
                + delimiter + std::to_string(leu.second) 
				+ delimiter + std::to_string(dleu) 
                + delimiter + std::to_string(lys.first)
                + delimiter + std::to_string(lys.second) 
				+ delimiter + std::to_string(dlys) 
                + delimiter + std::to_string(met.first)
                + delimiter + std::to_string(met.second) 
				+ delimiter + std::to_string(dmet) 
                + delimiter + std::to_string(phe.first)
                + delimiter + std::to_string(phe.second) 
				+ delimiter + std::to_string(dphe) 
                + delimiter + std::to_string(pro.first)
                + delimiter + std::to_string(pro.second) 
				+ delimiter + std::to_string(dpro) 
                + delimiter + std::to_string(ser.first)
                + delimiter + std::to_string(ser.second) 
				+ delimiter + std::to_string(dser) 
                + delimiter + std::to_string(thr.first)
                + delimiter + std::to_string(thr.second) 
				+ delimiter + std::to_string(dthr) 
                + delimiter + std::to_string(trp.first)
                + delimiter + std::to_string(trp.second) 
				+ delimiter + std::to_string(dtrp) 
                + delimiter + std::to_string(tyr.first)
                + delimiter + std::to_string(tyr.second) 
				+ delimiter + std::to_string(dtyr) 
                + delimiter + std::to_string(val.first)
                + delimiter + std::to_string(val.second) 
				+ delimiter + std::to_string(dval) 
                + delimiter + std::to_string(tiny.first)
                + delimiter + std::to_string(tiny.second)
                + delimiter + std::to_string(small.first)
                + delimiter + std::to_string(small.second) 
                + delimiter + std::to_string(aliphatic.first)
                + delimiter + std::to_string(aliphatic.second) 
                + delimiter + std::to_string(aromatic.first)
                + delimiter + std::to_string(aromatic.second) 
                + delimiter + std::to_string(nonpolar.first)
                + delimiter + std::to_string(nonpolar.second) 
                + delimiter + std::to_string(polar.first)
                + delimiter + std::to_string(polar.second) 
                + delimiter + std::to_string(charged.first)
                + delimiter + std::to_string(charged.second) 
                + delimiter + std::to_string(basic.first)
                + delimiter + std::to_string(basic.second) 
                + delimiter + std::to_string(acidic.first)
                + delimiter + std::to_string(acidic.second); 
        }
};

#endif
